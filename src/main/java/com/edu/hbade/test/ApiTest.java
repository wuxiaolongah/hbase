package com.edu.hbade.test;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;

public class ApiTest {
	//表名
    public static final String TABLE_NAME="table1";
    //列族名称
    public static final String FAMILY_NAME="family1";
    //行键
    public static final String ROW_KEY="rowkey1";
    
	
	public static void qqqmain(String[] args) throws Exception {
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.rootdir","hdfs://103.40.232.187:9000/hbase");
		conf.set("hbase.zookeeper.quorum", "103.40.232.187:2181,103.40.232.188:2181,103.40.232.189:2181");
		conf.set("hbase.master","103.40.232.187:60000");
		
		final HBaseAdmin hBaseAdmin = new HBaseAdmin(conf);
        
      //1、创建表，删除表使用HBaseAdmin
        //HBase shell : describe 'table1        
        createTable(hBaseAdmin);
//
//        //2、删除表
//        //HBase shell : list
//        //deleteTable(hBaseAdmin);
//
//
//        //3、插入记录，查询一条记录，遍历所有记录HTable   
//        //HBase shell : scan 'table1'
//        final HTable hTable = new HTable(conf,TABLE_NAME);
//        //插入
//        putRecord(hTable);
//
//        //方法抽取 shift+alt+m
//        //4、查询某条记录
//        getRecord(hTable);
//
//        //5、全表扫描
//        scanTable(hTable);
//
//        hTable.close();

	}
	
	
	
	

    public static void scanTable(final HTable hTable) throws Exception {
        Scan scan = new Scan();
        final ResultScanner scanner = hTable.getScanner(scan);
        for (Result result : scanner) {
            final byte[]  value = result.getValue(FAMILY_NAME.getBytes(), "name".getBytes());
            System.out.println(result);
            System.out.println(new String(value));
        }
    }


    public static void getRecord(final HTable hTable) throws Exception {
        Get get = new Get(ROW_KEY.getBytes());
        //hTable.get(get);
        final Result result = hTable.get(get);
        final byte[]  value = result.getValue(FAMILY_NAME.getBytes(), "age".getBytes());
        System.out.println(result);
        System.out.println(new String(value));
    }


    public static void putRecord(final HTable hTable) 
            throws Exception{

        Put  put= new Put(ROW_KEY.getBytes());
        put.add(FAMILY_NAME.getBytes(), "gender".getBytes(), "male".getBytes());
        hTable.put(put);


    }

    public static void deleteTable(final HBaseAdmin hBaseAdmin) throws Exception{
        hBaseAdmin.disableTable(TABLE_NAME);
        hBaseAdmin.deleteTable(TABLE_NAME);
    }



    public static void createTable(final HBaseAdmin hBaseAdmin) throws Exception{
        if(!hBaseAdmin.tableExists(TABLE_NAME))
        {
            HTableDescriptor tableDescriptor = new HTableDescriptor(TABLE_NAME);
            //添加列族
            HColumnDescriptor family = new HColumnDescriptor(FAMILY_NAME);
            tableDescriptor.addFamily(family);          
            hBaseAdmin.createTable(tableDescriptor);
        }
    }


}
