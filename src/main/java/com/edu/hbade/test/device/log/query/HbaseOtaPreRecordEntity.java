package com.edu.hbade.test.device.log.query;

public class HbaseOtaPreRecordEntity {
	
	private String rowKey;
	
	private String checkTime;
	private String downTime;
	private String upTime;
	
	private String downFailTime;
	private String downFailStatus;
	
	private String upFailTime;
	private String upFailStatus;
	
	private String status;
	
	public String getRowKey() {
		return rowKey;
	}
	public void setRowKey(String rowKey) {
		this.rowKey = rowKey;
	}
	public String getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}
	public String getDownTime() {
		return downTime;
	}
	public void setDownTime(String downTime) {
		this.downTime = downTime;
	}
	public String getUpTime() {
		return upTime;
	}
	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}
	public String getDownFailTime() {
		return downFailTime;
	}
	public void setDownFailTime(String downFailTime) {
		this.downFailTime = downFailTime;
	}
	public String getDownFailStatus() {
		return downFailStatus;
	}
	public void setDownFailStatus(String downFailStatus) {
		this.downFailStatus = downFailStatus;
	}
	public String getUpFailTime() {
		return upFailTime;
	}
	public void setUpFailTime(String upFailTime) {
		this.upFailTime = upFailTime;
	}
	public String getUpFailStatus() {
		return upFailStatus;
	}
	public void setUpFailStatus(String upFailStatus) {
		this.upFailStatus = upFailStatus;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "HbaseOtaPreRecordEntity [rowKey=" + rowKey + ", checkTime=" + checkTime + ", downTime=" + downTime
				+ ", upTime=" + upTime + ", downFailTime=" + downFailTime + ", downFailStatus=" + downFailStatus
				+ ", upFailTime=" + upFailTime + ", upFailStatus=" + upFailStatus + ", status=" + status + "]";
	}

	
}
