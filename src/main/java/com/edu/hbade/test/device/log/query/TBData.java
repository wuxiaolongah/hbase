package com.edu.hbade.test.device.log.query;

import java.util.List;
import java.util.Map;

class TBData {
	private Integer currentPage;
	private Integer pageSize;
	private Integer totalCount;
	private Integer totalPage;
	private List<HbaseOtaPreRecordEntity> resultList;

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}



	public List<HbaseOtaPreRecordEntity> getResultList() {
		return resultList;
	}

	public void setResultList(List<HbaseOtaPreRecordEntity> resultList) {
		this.resultList = resultList;
	}

	@Override
	public String toString() {
		return "TBData [currentPage=" + currentPage + ", pageSize=" + pageSize + ", totalCount=" + totalCount
				+ ", totalPage=" + totalPage + ", resultList=" + resultList + "]";
	}
	
	
}