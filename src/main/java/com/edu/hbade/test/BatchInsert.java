package com.edu.hbade.test;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;

public class BatchInsert {
	//表名
    public static final String TABLE_NAME="ota_pre_record";
    //列族名称
    public static final String FAMILY_NAME="info";
    //行键
    public static final String ROW_KEY="rowkey1";
    
    
    public static final String PRODUCTID="22222";
    public static final String DEVICEID="88888";
    
    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
	
	public static void main(String[] args) throws Exception {
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.rootdir","hdfs://192.168.1.20:9000/hbase");
		conf.set("hbase.zookeeper.quorum", "192.168.1.10:2181,192.168.1.11:2181,192.168.1.12:2181");
		conf.set("hbase.master","192.168.1.20:60000");
		
		final HBaseAdmin hBaseAdmin = new HBaseAdmin(conf);
        
		
		//deleteTable(hBaseAdmin);
		
      //1、创建表，删除表使用HBaseAdmin     
        createTable(hBaseAdmin);

       //3、插入记录，查询一条记录，遍历所有记录HTable   
        final HTable hTable = new HTable(conf,TABLE_NAME);
        List<Put> list = new ArrayList<Put>();
        
	
		Random r = new Random();
		Calendar c = Calendar.getInstance();
		Long tmp = null;
		Date date = null;
		String dateStr =null;
		
        for(int i=0;i<100;i++){
	
        	String key = PRODUCTID+ "+"  + DEVICEID + "+" +(Long.MAX_VALUE-System.currentTimeMillis()) + "+" +(12340+i);
        	Put  put1= new Put(key.getBytes());
        	Put  put2= new Put(key.getBytes());
        	Put  put3= new Put(key.getBytes());
        	Put  put4= new Put(key.getBytes());
        	Put  put5= new Put(key.getBytes());
        	Put  put6= new Put(key.getBytes());
        	Put  put7= new Put(key.getBytes());
        	Put  put8= new Put(key.getBytes());
        	
        	
    		tmp = System.currentTimeMillis()+(long) r.nextInt(1000000);
    		c.setTimeInMillis(tmp); 
    		date = c.getTime();
    		dateStr = sdf.format(date);
        	put1.add(FAMILY_NAME.getBytes(), "check_time".getBytes(), dateStr.getBytes());
        	
        	
    		tmp = System.currentTimeMillis()+(long) r.nextInt(1000000);
    		c.setTimeInMillis(tmp); 
    		date = c.getTime();
    		dateStr = sdf.format(date);
            put2.add(FAMILY_NAME.getBytes(), "down_time".getBytes(), dateStr.getBytes());
            
    		tmp = System.currentTimeMillis()+(long) r.nextInt(1000000);
    		c.setTimeInMillis(tmp); 
    		date = c.getTime();
    		dateStr = sdf.format(date);
            put3.add(FAMILY_NAME.getBytes(), "up_time".getBytes(), dateStr.getBytes());
            
            put8.add(FAMILY_NAME.getBytes(), "status".getBytes(), String.valueOf(r.nextInt(5)).getBytes() );
            list.add(put8);
            
            
            if(i%3 == 0){//模拟下载失败
            	tmp = System.currentTimeMillis()+(long) r.nextInt(1000000);
        		c.setTimeInMillis(tmp); 
        		date = c.getTime();
        		dateStr = sdf.format(date);
                put4.add(FAMILY_NAME.getBytes(), "down_fail_time".getBytes(), dateStr.getBytes());
                put5.add(FAMILY_NAME.getBytes(), "down_fail_status".getBytes(), String.valueOf(r.nextInt(4)).getBytes());
                list.add(put4);
                list.add(put5);
            }
                    
            if(i%7 == 0){//模拟升级失败
            	tmp = System.currentTimeMillis()+(long) r.nextInt(1000000);
        		c.setTimeInMillis(tmp); 
        		date = c.getTime();
        		dateStr = sdf.format(date);
                put6.add(FAMILY_NAME.getBytes(), "up_fail_time".getBytes(), dateStr.getBytes());
                put7.add(FAMILY_NAME.getBytes(), "up_fail_status".getBytes(), String.valueOf(r.nextInt(10)).getBytes());
                list.add(put6);
                list.add(put7);
            }
                    
            list.add(put1);
            list.add(put2);
            list.add(put3);
            

        }

        hTable.put(list);//批量插入
        hTable.close();
       
        
        System.out.println("===================ok======================");
	}
	
	
	
	

    public static void scanTable(final HTable hTable) throws Exception {
        Scan scan = new Scan();
        final ResultScanner scanner = hTable.getScanner(scan);
        for (Result result : scanner) {
            final byte[]  value = result.getValue(FAMILY_NAME.getBytes(), "name".getBytes());
            System.out.println(result);
            System.out.println(new String(value));
        }
    }


    public static void getRecord(final HTable hTable) throws Exception {
        Get get = new Get(ROW_KEY.getBytes());
        //hTable.get(get);
        final Result result = hTable.get(get);
        final byte[]  value = result.getValue(FAMILY_NAME.getBytes(), "age".getBytes());
        System.out.println(result);
        System.out.println(new String(value));
    }


    public static void putRecord(final HTable hTable) 
            throws Exception{

        Put  put= new Put(ROW_KEY.getBytes());
        put.add(FAMILY_NAME.getBytes(), "gender".getBytes(), "male".getBytes());
        hTable.put(put);


    }

    public static void deleteTable(final HBaseAdmin hBaseAdmin) throws Exception{
        hBaseAdmin.disableTable(TABLE_NAME);
        hBaseAdmin.deleteTable(TABLE_NAME);
    }



    public static void createTable(final HBaseAdmin hBaseAdmin) throws Exception{
        if(!hBaseAdmin.tableExists(TABLE_NAME))
        {
            HTableDescriptor tableDescriptor = new HTableDescriptor(TABLE_NAME);
            //添加列族
            HColumnDescriptor family = new HColumnDescriptor(FAMILY_NAME);
            tableDescriptor.addFamily(family);          
            hBaseAdmin.createTable(tableDescriptor);
        }
    }


}
