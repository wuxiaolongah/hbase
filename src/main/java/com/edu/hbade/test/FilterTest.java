package com.edu.hbade.test;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.util.Bytes;

import com.edu.hbade.test.device.log.query.HbaseOtaPreRecordEntity;

import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;

public class FilterTest {
	//表名
    public static final String TABLE_NAME="ota_pre_record";
    //列族名称
    public static final String FAMILY_NAME="info";
    
    
    public static final String PRODUCTID="22222";
    public static final String DEVICEID="88888";
    
	private static String family1 = "info";
	private static String qualifier1="check_time";
	private static String qualifier2="down_time";
	private static String qualifier3="up_time";
	private static String qualifier4="down_fail_time";
	private static String qualifier5="down_fail_status";
	private static String qualifier6="up_fail_time";
	private static String qualifier7="up_fail_status";
	private static String qualifier8="status";
    
    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
	
	public static void main(String[] args) throws Exception {
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.rootdir","hdfs://192.168.1.20:9000/hbase");
		conf.set("hbase.zookeeper.quorum", "192.168.1.10:2181,192.168.1.11:2181,192.168.1.12:2181");
		conf.set("hbase.master","192.168.1.20:60000");
		
		
		Connection connection=ConnectionFactory.createConnection(conf);
		Table table=connection.getTable(TableName.valueOf(TABLE_NAME));
		
//	    //全表扫描		
//	    scanTable(table);
		
		//   17  23   18  17  25
		
		
		
		
		
		//条件查询
		Scan scan =new Scan();
		FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);// MUST_PASS_ALL(条件 AND) MUST_PASS_ONE（条件OR）
		Filter filter = new SingleColumnValueFilter(getBytes("info"), getBytes("status"),CompareOp.EQUAL, getBytes("4"));
		filterList.addFilter(filter);
		scan.setFilter(filterList);	
		
		scan.setCaching(1000);
		scan.setCacheBlocks(false);
		ResultScanner scanner = table.getScanner(scan);
		int i = 0;
		List<byte[]> rowList = new LinkedList<byte[]>();//获得所有的rowKey的二进制码
		// 遍历扫描器对象， 并将需要查询出来的数据row key取出
		for (Result result : scanner) {//result中只有rowKey和check_time,没有其他的列,所以不能在这里封装所有的结果
			String row = toStr(result.getRow());		
			rowList.add(getBytes(row));
		}

		// 获取取出的row key的GET对象
		List<Get> getList = getList(rowList);
		Result[] results = table.get(getList);
		// 遍历结果
		List<HbaseOtaPreRecordEntity> beanList  = new LinkedList<HbaseOtaPreRecordEntity>();
		int num =0;
		for (Result result : results) {
			String tmpRowKey = toStr(result.getRow());
			Map<byte[], byte[]> fmap = packFamilyMap(result);
			HbaseOtaPreRecordEntity bean = packRowMap(fmap,tmpRowKey);
			beanList.add(bean);
			num++;
		}
        System.out.println(beanList);
		
	    table.close();   
        System.out.println("=================ok====================num: " + num );
	}
	
	
	
	
	/**
	 * 封装每行数据
	 */
	//private static Map<String, String> packRowMap(Map<byte[], byte[]> dataMap) {
	private static HbaseOtaPreRecordEntity packRowMap(Map<byte[], byte[]> dataMap,String tmpRowKey) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		HbaseOtaPreRecordEntity bean = new HbaseOtaPreRecordEntity();
		for (byte[] key : dataMap.keySet()) {
			byte[] value = dataMap.get(key);
			map.put(toStr(key), toStr(value));
		}	
		if(tmpRowKey != null){bean.setRowKey(tmpRowKey);}
		if(map.get(qualifier1) != null){bean.setCheckTime(map.get(qualifier1));}
		if(map.get(qualifier2) != null){bean.setDownTime(map.get(qualifier2));}
		if(map.get(qualifier3) != null){bean.setUpTime(map.get(qualifier3));}
		if(map.get(qualifier4) != null){bean.setDownFailTime(map.get(qualifier4));}
		if(map.get(qualifier5) != null){bean.setDownFailStatus(map.get(qualifier5));}
		if(map.get(qualifier6) != null){bean.setUpFailTime(map.get(qualifier6));}
		if(map.get(qualifier7) != null){bean.setUpFailStatus(map.get(qualifier7));}
		if(map.get(qualifier8) != null){bean.setStatus(map.get(qualifier8));}
			System.out.println(map.get(qualifier8));
		//return map;
		return bean;
	}
	/**
	 * 封装配置的所有字段列族
	 */
	private static Map<byte[], byte[]> packFamilyMap(Result result) {
		Map<byte[], byte[]> dataMap = new LinkedHashMap<byte[], byte[]>();
		dataMap.putAll(result.getFamilyMap(getBytes(family1)));
		return dataMap;
	}
	
	/* 根据ROW KEY集合获取GET对象集合 */
	private static List<Get> getList(List<byte[]> rowList) {
		List<Get> list = new LinkedList<Get>();
		for (byte[] row : rowList) {
			Get get = new Get(row);
			get.addColumn(getBytes(family1), getBytes(qualifier1));
			get.addColumn(getBytes(family1), getBytes(qualifier2));
			get.addColumn(getBytes(family1), getBytes(qualifier3));
			get.addColumn(getBytes(family1), getBytes(qualifier4));
			get.addColumn(getBytes(family1), getBytes(qualifier5));
			get.addColumn(getBytes(family1), getBytes(qualifier6));
			get.addColumn(getBytes(family1), getBytes(qualifier7));
			get.addColumn(getBytes(family1), getBytes(qualifier8));
			list.add(get);
		}
		return list;
	}
	
	private static String toStr(byte[] bt) {
		return Bytes.toString(bt);
	}
	
	/* 转换byte数组 */
	public static byte[] getBytes(String str) {
		if (str == null){
			str = "";
		}	
		return Bytes.toBytes(str);
	}

    public static void scanTable(Table table) throws Exception {
        Scan scan = new Scan();
        
        ResultScanner scanner=table.getScanner(scan);
        
        for (Result result : scanner) {
            HbaseOtaPreRecordEntity bean = new HbaseOtaPreRecordEntity();
            final byte[]  value1 = result.getValue(FAMILY_NAME.getBytes(), "check_time".getBytes());
            final byte[]  value2 = result.getValue(FAMILY_NAME.getBytes(), "down_time".getBytes());
            final byte[]  value3 = result.getValue(FAMILY_NAME.getBytes(), "up_time".getBytes());
            final byte[]  value4 = result.getValue(FAMILY_NAME.getBytes(), "down_fail_time".getBytes());
            final byte[]  value5 = result.getValue(FAMILY_NAME.getBytes(), "down_fail_status".getBytes());
            final byte[]  value6 = result.getValue(FAMILY_NAME.getBytes(), "up_fail_time".getBytes());
            final byte[]  value7 = result.getValue(FAMILY_NAME.getBytes(), "up_fail_status".getBytes());
            if(value1 != null)bean.setCheckTime(new String(value1));
            if(value2 != null)bean.setDownTime(new String(value2));
            if(value3 != null)bean.setUpTime(new String(value3));
            if(value4 != null)bean.setDownFailTime(new String(value4));
            if(value5 != null)bean.setDownFailStatus(new String(value5));
            if(value6 != null)bean.setUpFailTime(new String(value6));
            if(value7 != null)bean.setUpFailStatus(new String(value7));
            System.out.println(bean);
        }
    }


}
