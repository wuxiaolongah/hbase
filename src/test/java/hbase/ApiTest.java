package hbase;

import java.io.IOException;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;

public class ApiTest {
	public static void eeemain(String[] args) throws Exception {
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.rootdir","hdfs://hd1:9000/hbase");
		conf.set("hbase.zookeeper.quorum", "192.168.1.20:2181,192.168.1.21:2181,192.168.1.22:2181");
		
		HBaseAdmin admin = new HBaseAdmin((HBaseConfiguration)conf);
		
		//TableName table = TableName.valueOf("test3");
		//HTableDescriptor tdesc = new HTableDescriptor(table);
		HTableDescriptor tdesc = new HTableDescriptor("test3");
		
		HColumnDescriptor  cdesc = new HColumnDescriptor("data");
		tdesc.addFamily(cdesc);
		
		admin.createTable(tdesc);	
		System.out.println("=====================建表完成");
		
		
//		HTable ht = new HTable((HBaseConfiguration) conf, "test3");
//		Put put = new Put(Bytes.toBytes("row1"));
//		put.add(Bytes.toBytes("data"), Bytes.toBytes("1"), Bytes.toBytes("wu"));
//		put.add(Bytes.toBytes("data"), Bytes.toBytes("2"), Bytes.toBytes("xiao"));
//		put.add(Bytes.toBytes("data"), Bytes.toBytes("3"), Bytes.toBytes("long"));
//		
//		ht.put(put);
//		//ht.close();
//		System.out.println("=================数据插入成功");
//		
//		//封装查询条件
//		Get get = new Get(Bytes.toBytes("row1"));
//		get.addColumn(Bytes.toBytes("data"), Bytes.toBytes("1"));
//		get.addColumn(Bytes.toBytes("data"), Bytes.toBytes("2"));
//		//查询
//		Result res = ht.get(get);
//		KeyValue kv = res.getColumnLatest(Bytes.toBytes("data"), Bytes.toBytes("1"));
//		System.out.println("data:1:key"+Bytes.toString(kv.getKey()));
//		System.out.println("data:1:value"+Bytes.toString(kv.getValue()));
//		//ht.close();
//		System.out.println("=================数据查询成功");
//		
//		
//		Scan scan = new Scan();
//		ResultScanner  rs = ht.getScanner(scan);
//		for(Result r : rs){
//			System.out.println(Bytes.toString(r.getRow())+":"+Bytes.toString(r.getColumnLatestCell(Bytes.toBytes("data"), Bytes.toBytes("1")).getValue()));
//		}
//		rs.close();
//		//ht.close();
//		System.out.println("=================全表扫描成功");
//	
//		
//		Delete d = new Delete(Bytes.toBytes("row1"));
//		ht.delete(d);
//		//ht.close();
//		System.out.println("========删除一行数据");
//		
//		
//		admin.disableTable("test3");
//		admin.deleteTable("test3");
//		admin.close();
//		System.out.println("========删除表");
	
	}
	
	

}
